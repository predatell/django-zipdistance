from django.db import models
from django.db.models import Aggregate
#from django.db.models.sql.aggregates import Aggregate as AggregateImpl

#class DistanceFromImpl(AggregateImpl): 
#    sql_function = ''
#    is_computed = True
#    is_ordinal = True

#    sql_template = ('3959 * acos( cos( radians(%(t_lat)f) ) * cos( radians( latitude ) ) * '
#                    'cos( radians( longitude ) - radians(%(t_lon)f) ) + sin( radians(%(t_lat)f) ) * '
#                    'sin( radians( latitude ) ) )')

#    def as_sql(self, qn, connection):
#        "Return the aggregate, rendered as SQL."
#        return self.sql_template % self.extra, []

        
class DistanceFrom(Aggregate):
    name = "DistanceFromImpl"
    template = ('3959 * acos( cos( radians(%(t_lat)f) ) * cos( radians( latitude ) ) * '
                'cos( radians( longitude ) - radians(%(t_lon)f) ) + sin( radians(%(t_lat)f) ) * '
                'sin( radians( latitude ) ) )')   
    
#    def add_to_query(self, query, alias, col, source, is_summary):
#        aggregate = DistanceFromImpl(col, source=source, is_summary=is_summary, **self.extra)
#        query.aggregates[alias] = aggregate

    def as_sql(self, compiler, connection):
        return self.template % self.extra, []

    
class ZipDistanceManager(models.Manager):

    def distance_from(self, target, limit = 0):
        qs = self.annotate(distance = DistanceFrom('zipcode', t_lon=target.longitude, t_lat=target.latitude))
        if bool(limit):
            qs = qs.filter(distance__lte = float(limit))
        qs = qs.order_by('distance')
        return qs


class ZipDistance(models.Model):
    zipcode = models.CharField(max_length = 5, unique = True)
    latitude = models.FloatField()
    longitude = models.FloatField()

    objects = ZipDistanceManager()

    class Meta:
        ordering = ['zipcode']

    def __unicode__(self):
        return self.zipcode

    def distance_between(self, other):
        return self.__class__.objects.distance_from(self).get(zipcode = other.zipcode).distance

    
        
